// Core modules
import PropTypes, { array } from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Link, Prompt } from 'react-router-dom';
import treeChanges from 'tree-changes';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Select from 'react-select';
import AsyncSelect from 'react-select/lib/Async';
import { Radio, RadioGroup } from 'react-icheck';
import { Table } from 'react-bootstrap';
import S3FileUpload from 'components/react-s3';

// Configurations and actions
import config from 'config';
import { Themes, DAYS_PER_WEEK, MEDIA_URL, MEDIA_DIR, S3_MEDIA_URL, S3_UPLOAD_CONFIG } from 'constants/index';
import { API } from 'constants/api';
import { appGet, appPost, showAlert, recipeReset, recipeStoreUpdate } from 'actions/index';
import images from 'images';

// Components
// import Logo from 'components/Logo';
import FileBase64 from 'components/FileBase64';
import Breadcrumb from 'components/Breadcrumb';
import PreLoader from 'components/PreLoader';
import translator from 'modules/translator';
import { searchIngredients, strToFloat, displayApproved, rolePermission, getImageName, nutritionValue } from 'modules/helpers';

const intialState = {
  formData: {
    is_pro: 0,
    active: 1,
    approved: '',
    ingredient: [],
    category: {},
    days_a_week: 1,
    persons: '',
    preparation_minutes: '',
    title: '',
    image: '',
    recipesCategoriesId: [],
    nutritionscheduleMeals: [],
    allergensIds: [],
    holidays: [],
    my_mood: [],
    food_preferance: [],
    season: [],
    preparation: '',
    information: '',
    similar: '',
    direction: [{
      steps: '',
      image: ''
    }],
  },
  imageFile: '',
  fileError: null,
  ingredient: [],
  all_images_list: [],
  s3loader: false,
  ingTotals: {
    carbs: 0,
    protein: 0,
    fat: 0,
    fiber: 0,
    kcal: 0,
  },
  shouldBlockNavigation: true
};

const formValiation = {
  ingredient: { message: 'Ingredient should be mandatory' },
  title: { message: 'Recipe title should be mandatory' },
  preparation_minutes: { message: 'Preperation should be mandatory' },
  persons: { message: 'Number of persons should be mandatory' },
  information: { message: 'Information should be mandatory' },
  // preparation: { message: 'Preperation steps should be mandatory' },
  similar: { message: 'Similar should be mandatory' },
  days_a_week: { message: 'Numbers of days should be mandatory' },
  image: { message: 'At least 1 image should be mandatory' },
  food_preferance: { message: 'Food preference should be mandatory' },
  nutritionscheduleMeals: { message: 'Meal should be mandatory' },
  // category: { message: 'Category should be mandatory' },
  // recipesCategoriesId: { message: 'Filters should be mandatory' },
  allergensIds: { message: 'Allergens should be mandatory' },
  // season: { message: 'Seasons should be mandatory' },
};

export class CreateRecipe extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = { ...intialState };
    this.handleFormSave = this.handleFormSave.bind(this);
    this.editorConfig = {
      toolbar: ['Heading', '|', 'Bold', 'Italic', 'Alignment', 'bulletedList', 'numberedList', 'Link', 'BlockQuote', 'Undo', 'Redo'],
    };
  }

  static propTypes = {
    app: PropTypes.object,
    appConfig: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    match: PropTypes.object,
    recipes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
  };

  componentDidMount() {
    const { match: { params }, dispatch } = this.props;
    // this.getRecipes({ page: params.page, path  });
    dispatch(appGet({ ...API.RECIPE.OPTIONS, callback: params.recipe_id ? 'GET' : null }));
  }


  componentWillReceiveProps(nextProps) {
    const { params } = nextProps.match;
    const { recipes } = nextProps;
    const { dispatch, history } = this.props;
    const { changedTo, changed } = treeChanges(this.props, nextProps);
    const { uri } = API.RECIPE.GET;

    // Reset video form after add new video. 
    if (changedTo('app.status', true) && changedTo('app.callback', 'success')) {
      this.preperationContent = '';
      this.informationContent = '';
      this.similarContent = '';
      this.setState({ ...intialState });
    }

    // get user profile which need to edit after user form options API
    if (changedTo('recipes.callback', 'GET') && params.recipe_id) {
      dispatch(appGet({ ...API.RECIPE.GET, uri: `${uri}/${params.recipe_id}` }));
    }

    // update user profile which need to edit
    if (changedTo('app.callback', 'redirect')) {
      this.setState({ shouldBlockNavigation: false }, () => history.push('/manage/recipes'));
      return false;
    }

    if (changed('recipes.recipeInfo.recipe.instance_id') && params.recipe_id) {
      let formData = {};
      let except = ['category', 'food_preferance', 'season'];
      const { recipe, allergensIds, holidays, my_mood, direction } = recipes.recipeInfo;
      Object.keys(intialState.formData).map(k => { if (recipe.hasOwnProperty(k) && except.indexOf(k) === -1) formData[k] = recipe[k]; });

      // console.log('formData', formData, recipes);

      this.preperationContent = recipe.preparation;
      this.informationContent = recipe.information;
      this.similarContent = recipe.similar;
      recipes.options.category.map(item => {
        if (item.instance_id == recipe.category) {
          formData.category = { label: item.name, value: item.instance_id };
        }
      });
      const dayMeals = recipes.recipeInfo.dayMeals.map(item => item.nutritionschedule_meal_id);
      formData.nutritionscheduleMeals = [];
      recipes.options.meals.map(item => {
        if (dayMeals.length && dayMeals.indexOf(item.instance_id) >= 0) {
          formData.nutritionscheduleMeals.push({
            label: item.displayType,
            value: item.instance_id,
          });
        }
      });
      formData.recipesCategoriesId = [];
      const recipesCategoriesId = recipes.recipeInfo.recipe_category.map(item => item.categorie_id);
      recipes.options.filter.map(item => {
        if (recipesCategoriesId.length && recipesCategoriesId.indexOf(item.instance_id) >= 0) {
          formData.recipesCategoriesId.push({
            label: item.name,
            value: item.instance_id,
          });
        }
      });
      // recipes.options.season.map(item => {
      //   if (item.instance_id == recipe.season) {
      //     formData.season = { label: item.name, value: item.instance_id };
      //   }
      // });

      if (recipe.food_preferance) {
        // formData.food_preferance = { label: recipe.food_preferance.name, value: recipe.food_preferance.instance_id };
        formData.food_preferance = [];
        recipe.food_preferance.map(item => formData.food_preferance.push({ label: item.name, value: item.food_prefrance_id }));
      }

      if (allergensIds.length) {
        formData.allergensIds = allergensIds.map(item => ({ label: item.alergian_name, value: item.alergian_id }));
      }

      if (holidays.length) {
        formData.holidays = holidays.map(item => ({ label: item.name, value: item.holiday_id }));
      }

      if (my_mood.length) {
        formData.my_mood = my_mood.map(item => ({ label: item.name, value: item.my_mood_id }));
      }

      if (recipe.season) {
        // formData.season = { label: recipe.season.name, value: recipe.season.instance_id };
        formData.season = [];
        recipe.season.map(item => formData.season.push({ label: item.name, value: item.seasons_id }));
      }


      formData.direction = direction && direction.length ? direction : intialState.formData.direction;



      let ingredients = [];
      recipes.recipeInfo.recipe_ingredient.map(item => {
        const perGram = item.detail.unit && item.detail.unit.length ? (item.detail.unit[0].size == 0 ? 100 : 1) : 1;
        const perGramSize = item.detail.unit && item.detail.unit.find(u => item.portiegrootte_id == u.portiegrootte_id);
        console.log("perGramSize", perGramSize, item.portiegrootte_id);
        ingredients.push({
          // label: item.detail.product_description,
          // value: item.ingredient_id,
          // info: item.detail,
          portiegroote: item.portiegrootte_id,
          perGram: item.free_value ? item.free_value : perGram,
          perGramSize: perGramSize ? (perGramSize.size == 0 ? 1 : perGramSize.size) : 100,
          ...item.detail,
        });

      });
      console.log("formData", formData, ingredients);
      this.calculateNetrition(ingredients);
      // ingredient
      this.setState({ formData, ingredient: ingredients, all_images_list: [] });
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    const { shouldBlockNavigation } = this.state;
    if (shouldBlockNavigation) {
      window.onbeforeunload = () => true;
    } else {
      window.onbeforeunload = undefined;
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(recipeReset('recipeInfo'));
    window.onbeforeunload = undefined;
  }

  validateRecipe = (payload) => {
    let status = true;
    let message = null;
    const { match: { params }, recipes: { recipeInfo } } = this.props;
    let except = [];
    if (params.recipe_id && recipeInfo.all_images.length) {
      except = ['image'];
    };
    const keys = Object.keys(formValiation);
    for (let i = 0; i < keys.length; i++) {
      if (except.indexOf(keys[i]) < 0) {
        const param = payload[keys[i]];
        if (param instanceof Array) {
          if (param.length == 0) {
            status = false;
            message = formValiation[keys[i]].message;
            break;
          }
        }
        else if (!param) {
          status = false;
          message = formValiation[keys[i]].message;
          break;
        }
      }

    }
    return { status, message };
  }

  s3fileNames = [];

  _s3FileUpload = (file, index) => {
    const { all_images_list } = this.state;
    const newName = getImageName(file.name);
    const s3Config = { ...S3_UPLOAD_CONFIG, dirName: `${S3_UPLOAD_CONFIG.dirName}/${MEDIA_DIR.RECIPE}` };
    S3FileUpload
      .uploadFile(file, s3Config, { newName })
      .then(data => {
        console.log(data);
        // const { result } = data;
        this.s3fileNames.push(newName);
        index++;
        if (index < all_images_list.length) {
          this._s3FileUpload(all_images_list[index].file, index);
        }
        else {
          this.saveRecipe();
        }

      })
      .catch(err => {
        console.error(err);
        index++;
        if (index < all_images_list.length) {
          this._s3FileUpload(all_images_list[index].file, index);
        }
        else {
          this.saveRecipe();
        }
      });
  }

  handleFormSave = (e) => {
    e.preventDefault();
    const { dispatch, match: { params } } = this.props;
    const { formData, ingredient, all_images_list } = this.state;
    const { category, recipesCategoriesId, nutritionscheduleMeals, allergensIds, season, food_preferance, holidays, my_mood } = formData;
    // console.log(formData, ingredient, recipesCategoriesId, allergensIds, nutritionscheduleMeals, all_images_list);

    let payload = {
      ...formData,
      ingredient: ingredient ? ingredient.map(item => ({ id: item.instance_id, portiegroote: item.portiegroote, perGram: item.perGram })) : [],
      image: all_images_list.length ? all_images_list.map(item => item.file) : [],
      preparation: this.preperationContent,
      information: this.informationContent,
      similar: this.similarContent,
      category: 0, // category.value,
      recipesCategoriesId: recipesCategoriesId ? recipesCategoriesId.map(item => item.value) : [],
      nutritionscheduleMeals: nutritionscheduleMeals ? nutritionscheduleMeals.map(item => item.value) : [],
      holidays: holidays ? holidays.map(item => item.value) : [],
      my_mood: my_mood ? my_mood.map(item => item.value) : [],
      allergensIds: allergensIds ? allergensIds.map(item => item.value) : [],
      season: season ? season.map(item => item.value) : [],
      food_preferance: food_preferance ? food_preferance.map(item => item.value) : [],
    };
    console.log(payload);

    const isValid = this.validateRecipe(payload);
    if (isValid.status) {
      this.recipePayload = payload;
      if (payload.image.length) {
        this.setState({ s3loader: true });
        this._s3FileUpload(payload.image[0], 0);
      }
      else {
        this.saveRecipe();
      }
    }
    else {
      dispatch(showAlert(isValid.message, { type: 'error' }));
    }

  };

  saveRecipe = () => {
    this.setState({ s3loader: false, all_images_list: [] });
    const { dispatch, match: { params } } = this.props;
    const payload = { ...this.recipePayload, image: this.s3fileNames };
    if (params.recipe_id) {
      const { uri } = API.RECIPE.UPDATE;
      dispatch(appPost({ ...API.RECIPE.UPDATE, payload, uri: `${uri}/${params.recipe_id}` }));
    }
    else {
      dispatch(appPost({ ...API.RECIPE.CREATE, payload }));
    }
  }

  handleFormChange = (event) => {
    const { formData } = this.state;
    const { target } = event;
    console.log('target.value', target.value, target.validity);
    if (!target.validity.valid && target.value != '') { return; }
    console.log('target.value 1', target.value, target.type);
    const value = target.type === 'checkbox' || target.type === 'radio' ? parseInt(target.value, 10) : target.value;
    const name = target.name;
    console.log('target.value 2', value, name);
    this.setState({
      formData: {
        ...formData,
        [name]: value,
      }
    });

  };

  handleMultiSelectChange = (value, key) => {
    const { formData } = this.state;
    this.setState({
      formData: {
        ...formData,
        [key]: value,
      }
    });
  }

  onStGmChange = (e, index) => {
    const { value } = e.target;
    const { ingredient } = this.state;
    if (value.trim() == '' || value == 0) {
      const { portiegroote } = ingredient[index];
      let newIngredient = [...ingredient];
      const unit = ingredient[index].unit.find(u => u.portiegrootte_id == portiegroote) || ingredient[index].unit[0];
      // console.log(unit, index, newIngredient[index]);
      newIngredient[index].perGram = unit ? (unit.size == 0 ? 100 : 1) : 1;
      newIngredient[index].perGramSize = unit.size ? unit.size : 1;
      this.setState({ ingredient: newIngredient }, () => this.calculateNetrition(newIngredient));
    }

  }

  onRemove = (e, index) => {
    e.preventDefault();
    console.log(index);
    const { ingredient } = this.state;
    const newIngredient = ingredient.filter((item, i) => index != i);
    this.calculateNetrition(newIngredient);
    this.setState({
      ingredient: newIngredient,
    });
  }

  calculateNetrition = (newIngredient) => {
    let ingTotals = { ...intialState.ingTotals };
    newIngredient.map(item => {
      ingTotals.carbs += parseFloat(nutritionValue(item.carbohydrate, item.perGram, item.perGramSize));
      ingTotals.protein += parseFloat(nutritionValue(item.protein_total, item.perGram, item.perGramSize));
      ingTotals.fat += parseFloat(nutritionValue(item.fat_total, item.perGram, item.perGramSize));
      ingTotals.fiber += parseFloat(nutritionValue(item.fibre_total, item.perGram, item.perGramSize));
      ingTotals.kcal += parseFloat(nutritionValue(item.energy_kcal, item.perGram, item.perGramSize));
    });
    this.setState({ ingTotals });
  }

  getFiles = files => {
    console.log("files", files);
    const { dispatch, recipes } = this.props;
    const { all_images_list } = this.state;
    const { all_images } = recipes.recipeInfo;
    if (files.success) {
      const counter = 3 - ((all_images ? all_images.length : 0) + all_images_list.length);
      const allFiles = [];
      files.allFiles.map((item, index) => {
        console.log('all_images_list 1', index, counter);
        if (index < counter) {
          console.log('all_images_list 2', counter);
          allFiles.push(item);
        }
      });
      console.log('all_images_list', allFiles, files.allFiles);
      this.setState({ all_images_list: [...all_images_list, ...allFiles] });
    }
    else {
      dispatch(showAlert(files.message, { type: 'error' }));
    }

  }

  removeImage = (e, item, index = null) => {
    e.preventDefault();
    const { dispatch, recipes } = this.props;
    const { all_images_list } = this.state;
    const { uri } = API.RECIPE.IMAGE_DELETE;
    if (confirm('do you want to delete recipe image?')) {
      if (item.id) {
        const { recipeInfo } = recipes;        
        dispatch(appPost({ ...API.RECIPE.IMAGE_DELETE, uri: `${uri}/${item.id}` }));
        let newRecipeInfo = JSON.parse(JSON.stringify(recipeInfo));
        let new_all_images = [...newRecipeInfo.all_images]; // JSON.parse(JSON.stringify(recipeInfo));
        // console.log(new_all_images);
        new_all_images.splice(index, 1);
        dispatch(recipeStoreUpdate({ root: 'recipeInfo', key: 'all_images', data: new_all_images }));
      }
      else {
        let newList = [...all_images_list];
        newList.splice(index, 1);
        console.log(index, newList);
        this.setState({ all_images_list: newList });
      }
    }
  };

  getFilePath = (item, key) => {
    let element = null;
    if (item.base64) element = <div className="auto-resize-landscape" key={`preview_${key}`}><img className="img-responsive margin" width="200" src={item.base64} alt="preview" /><a onClick={(e) => this.removeImage(e, item, key)} href="" className="remove_image"><i className="fa fa-times"></i></a></div>;
    else if (item.image_name) element = <div className="auto-resize-landscape" key={`preview_${key}`}><PreLoader key={item.image_name} src={`${S3_MEDIA_URL}/${MEDIA_DIR.RECIPE}/${item.image_name}`} /><a onClick={(e) => this.removeImage(e, item, key)} href="" className="remove_image"><i className="fa fa-times"></i></a></div>;
    return element;
  }

  promiseOptions = inputValue => (
    new Promise(resolve => {
      console.log('inputValue', inputValue);
      if (inputValue.length >= 3) {
        resolve(searchIngredients(inputValue));
      }
      else {
        resolve([]);
      }
    }));

  
  chooseIngredient = item => {
    const { ingredient } = this.state;
    console.log('item', item);
    const { info } = item;
    const newItem = {
      portiegroote: info.unit.length ? info.unit[0].portiegrootte_id : 1,
      perGram: info.unit.length ? info.unit[0].default_value : 1,
      perGramSize: info.unit.length ? (info.unit[0].size == 0 ? 1 : info.unit[0].size) : 100,
      ...item.info
    };
    console.log('item', newItem);
    const newIngredient = [...ingredient, ...[{ ...newItem }]];
    this.setState({
      ingredient: newIngredient,
    }, () => this.calculateNetrition(newIngredient));
  }

  onChangeIngs = (e, index, sizeChange) => {
    const { value, name } = e.target;
    const { ingredient } = this.state;

    const unit = ingredient[index].unit.find(u => u.portiegrootte_id == value) || ingredient[index].unit[0];
    const perGram = unit ? (unit.size == 0 ? 100 : 1) : 1;
    const newItem = { ...ingredient[index], [name]: value };
    if (sizeChange) {
      newItem.perGram = perGram;
      newItem.perGramSize = unit.size ? unit.size : 1;
    }
    const newIngredient = [...ingredient];
    newIngredient[index] = newItem;
    this.setState({
      ingredient: newIngredient,
    }, () => this.calculateNetrition(newIngredient));
  }

  addRange = (e) => {
    const { formData } = this.state;
    const range = formData.direction.concat([{
      steps: '',
      image: null
    }]);
    this.setState({
      formData: {
        ...formData,
        direction: range,
      }
    });
  };

  removeRange = (i) => {
    const { formData } = this.state;
    const { dispatch } = this.props;
    const { uri } = API.RECIPE.STEP_DELETE;
    const removeItem = formData.direction[i];
    if (confirm('do you want to remove this step?')) {
      const range = formData.direction.filter((item, index) => {
        return index != i;
      });
      if (removeItem.id) {
        dispatch(appPost({ ...API.RECIPE.STEP_DELETE, uri: `${uri}/${removeItem.id}` }));
      }
      this.setState({
        formData: {
          ...formData,
          direction: range,
        }
      });
    }
  }

  handleRangeChange = (event, index) => {
    const { target } = event;
    const { formData } = this.state;
    console.log(target.validity.valid, event);
    if (!target.validity.valid && target.value != '') { return; }
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    let range = [];
    formData.direction.find((item, i) => {
      if (i == index) range.push({ ...item, [name]: value });
      else range.push({ ...item });
    });
    this.setState({
      formData: {
        ...formData,
        direction: range,
      }
    });
  }

  getPageTitle = (recipe_id, isEditable) => {
    if (!isEditable) return translator.convert('heading_info_recipe');
    if (recipe_id) return translator.convert('heading_update_recipe');
    return translator.convert('heading_add_recipe');
  }

  render() {
    const { user: { auth }, appConfig, recipes, app, match: { params } } = this.props;
    const { theme } = appConfig;
    const { options, recipeInfo } = recipes;
    const all_images = params.recipe_id && recipeInfo.all_images ? recipeInfo.all_images : [];
    const { shouldBlockNavigation, formData, ingredient, all_images_list, s3loader, ingTotals } = this.state;
    const isEditable = params.recipe_id ? rolePermission(recipeInfo.recipe, auth) : true;
    const pageTitle = this.getPageTitle(params.recipe_id, isEditable);
    // console.log('formdata', formData);
    return (
      <div key="createRecipe" className="content-wrapper">
        {(s3loader || app.isLoading) && <div className="full-screen-loading"></div>}
        <Breadcrumb title={pageTitle} />
        <Prompt
          when={shouldBlockNavigation}
          message="Change you made may not be saved"
        />
        {/* <!-- Main content --> */}
        <section className="content">
          <form className="form-horizontal" onSubmit={this.handleFormSave}>
            <div className="row">
              <div className="col-md-12">
                <div className={`box ${theme.boxClass}`}>
                  <div className="box-header with-border">
                    <h3 className="box-title">Ingredients</h3>
                  </div>
                  <div className="box-body">
                    {isEditable && <div className="form-group required">
                      <label className="col-sm-3 pull-left control-label">{translator.convert('add_recipe_search_ingredients')}</label>
                      <div className="col-sm-5">
                        <AsyncSelect
                          cacheOptions
                          defaultOptions
                          loadOptions={this.promiseOptions}
                          name="ingredient"
                          onChange={(val) => this.chooseIngredient(val)}
                        />
                      </div>
                    </div>}
                    <div className="form-group">

                      <div className="col-sm-12">
                        <Table responsive hover bordered>
                          <thead>
                            <tr>
                              <th>{translator.convert('ing_tbl_ingredient')}</th>
                              <th>{translator.convert('ing_tbl_unit')}</th>
                              <th>{translator.convert('ing_tbl_per_gram')}</th>
                              <th>{translator.convert('ing_tbl_protein')}</th>
                              <th>{translator.convert('ing_tbl_fat')}</th>
                              <th>{translator.convert('ing_tbl_carbs')}</th>
                              <th>{translator.convert('ing_tbl_fiber')}</th>
                              <th>{translator.convert('ing_tbl_energy')}</th>
                              {isEditable && <th>{translator.convert('actions')}</th>}
                            </tr>
                          </thead>
                          <tbody>
                            {ingredient.length ? ingredient.map((item, index) => (<tr key={`choosen_ingredients_${index}`}>
                              <td>{item.product_description}</td>
                              <td>
                                <select className="form-control" name="portiegroote" value={item.portiegroote} onChange={(e) => this.onChangeIngs(e, index, true)} disabled={!isEditable}>
                                  {item.unit.map((u, i) => <option key={`ingredient_unit_${i}`} value={u.portiegrootte_id}>{u.unit_name || u.name}</option>)}
                                </select>
                              </td>
                              <td><input type="text" disabled={!isEditable} name="perGram" onBlur={(e) => this.onStGmChange(e, index)} onChange={(e) => this.onChangeIngs(e, index, false)} value={item.perGram} className="form-control" style={{ width: 120 }} /></td>
                              <td>{nutritionValue(item.protein_total, item.perGram, item.perGramSize)}</td>
                              <td>{nutritionValue(item.fat_total, item.perGram, item.perGramSize)}</td>
                              <td>{nutritionValue(item.carbohydrate, item.perGram, item.perGramSize)}</td>
                              <td>{nutritionValue(item.fibre_total, item.perGram, item.perGramSize)}</td>
                              <td>{nutritionValue(item.energy_kcal, item.perGram, item.perGramSize)}</td>
                              {isEditable && <td><a title="Delete" onClick={(e) => this.onRemove(e, index)} className="btn btn-danger btn-xs"><i className="fa fa-trash" ></i></a></td>}
                            </tr>)) : (<tr><td colSpan={9} >No ingredients choosen yet.</td></tr>)}
                            {ingredient.length ? <tr><td colSpan={3}></td>
                              <td><b>{strToFloat(ingTotals.protein)}</b></td>
                              <td><b>{strToFloat(ingTotals.fat)}</b></td>
                              <td><b>{strToFloat(ingTotals.carbs)}</b></td>
                              <td><b>{strToFloat(ingTotals.fiber)}</b></td>
                              <td><b>{strToFloat(ingTotals.kcal)}</b></td><td></td></tr> : ''}
                          </tbody>
                        </Table>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className={`box ${theme.boxClass}`}>
                  <div className="box-header with-border">
                    <h3 className="box-title">General info</h3>
                  </div>

                  <div className="box-body">
                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('title')}</label>
                      <div className="col-sm-5">
                        <input type="text" name="title" onChange={this.handleFormChange.bind(this)} value={formData.title} className="form-control" required disabled={!isEditable} />
                      </div>
                    </div>


                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_pre_time')}</label>
                      <div className="col-sm-5">
                        <input type="text" pattern="^\d*(\.\d{0,2})?$" name="preparation_minutes" onChange={this.handleFormChange.bind(this)} value={formData.preparation_minutes} className="form-control" required
                          disabled={!isEditable} />
                      </div>
                    </div>

                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_persons')}</label>
                      <div className="col-sm-5">
                        <input type="text" name="persons" onChange={this.handleFormChange.bind(this)} value={formData.persons} className="form-control" required disabled={!isEditable} />
                      </div>
                    </div>

                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_information')}</label>
                      <div className="col-sm-5">
                        {/* <input type="text" name="information" onChange={this.handleFormChange.bind(this)} value={formData.information} className="form-control" required /> */}
                        <CKEditor
                          editor={ClassicEditor}
                          config={this.editorConfig}
                          data={this.informationContent}
                          onInit={editor => {
                            // editor.isReadOnly = !isEditable;
                          }}
                          onChange={(event, editor) => {
                            const data = editor.getData();
                            this.informationContent = data;
                          }}
                        />
                      </div>
                    </div>
                    {/* <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_preperation')}</label>
                      <div className="col-sm-6">
                        <CKEditor
                          editor={ClassicEditor}
                          config={this.editorConfig}
                          data={this.preperationContent}
                          onInit={editor => {
                            editor.isReadOnly = !isEditable;
                          }}
                          onChange={(event, editor) => {
                            const data = editor.getData();
                            this.preperationContent = data;
                          }}
                        />
                      </div>
                    </div> */}
                    <div className="form-group">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_preperation')}</label>
                      <div className="col-sm-8">
                        {formData.direction && formData.direction.length ? formData.direction.map((item, i) => (<div className="row margin-t-10" key={`steps_${i}`}><div className="col-sm-8"><textarea className="form-control" name="steps" placeholder={`Step ${i + 1}`} value={item.steps} onInput={(e) => this.handleRangeChange(e, i)}></textarea></div><div className="col-sm-4">{i === 0 && <span className="input-group-addon" style={{ width: 50 }} onClick={this.addRange.bind(this)}><i className="fa fa-plus"></i></span>}
                          {i !== 0 && <span style={{ width: 50 }} className="input-group-addon" onClick={() => this.removeRange(i)}><i className="fa fa-trash"></i></span>}</div>
                        </div>)) : ''}
                      </div>
                    </div>

                    <div className="form-group">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_similar')}</label>
                      <div className="col-sm-5">
                        {/* <input type="text" name="similar" onChange={this.handleFormChange.bind(this)} value={formData.similar} className="form-control" required /> */}
                        <CKEditor
                          editor={ClassicEditor}
                          config={this.editorConfig}
                          data={this.similarContent}
                          onInit={editor => {
                            // editor.isReadOnly = !isEditable;
                          }}
                          onChange={(event, editor) => {
                            const data = editor.getData();
                            this.similarContent = data;
                          }}
                        />
                      </div>
                    </div>
                    {/*
                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('only_for_pro')}</label>
                      <div className="col-sm-5">
                        <RadioGroup name="is_pro" value={formData.is_pro}>
                          <Radio
                            value={1}
                            name="is_pro"
                            disabled={!isEditable}
                            radioClass="iradio_square-blue"
                            increaseArea="20%"
                            label=" <span class='label1'> Enable </span>"
                            onClick={(e) => {
                              this.handleFormChange(e);
                            }}
                          />
                          <Radio
                            value={0}
                            name="is_pro"
                            disabled={!isEditable}
                            radioClass="iradio_square-blue local_iradio_square"
                            increaseArea="20%"
                            label=" <span class='label1'> disable</span>"
                            onClick={(e) => this.handleFormChange(e)}
                          />
                        </RadioGroup>
                      </div>
                    </div>

                    */}
                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('active')}</label>
                      <div className="col-sm-6">
                        <RadioGroup name="active" value={formData.active}>
                          <Radio
                            value={1}
                            name="active"
                            disabled={!isEditable}
                            radioClass="iradio_square-blue"
                            increaseArea="20%"
                            label=" <span class='label1'> Active </span>"
                            onClick={(e) => {
                              this.handleFormChange(e);
                            }}
                          />
                          <Radio
                            value={0}
                            name="active"
                            disabled={!isEditable}
                            radioClass="iradio_square-blue local_iradio_square"
                            increaseArea="20%"
                            label=" <span class='label1'> Deactive</span>"
                            onClick={(e) => this.handleFormChange(e)}
                          />
                        </RadioGroup>
                      </div>
                    </div>
                    {params.recipe_id && displayApproved(recipeInfo.recipe ? recipeInfo.recipe : {}, auth) && <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('approved_by_admin')}</label>
                      <div className="col-sm-6">
                        <RadioGroup name="approved" value={formData.approved}>
                          <Radio
                            value={1}
                            name="approved"
                            disabled={!isEditable}
                            radioClass="iradio_square-blue"
                            increaseArea="20%"
                            label=" <span class='label1'> Approved </span>"
                            onClick={(e) => {
                              this.handleFormChange(e);
                            }}
                          />
                          <Radio
                            value={0}
                            name="approved"
                            disabled={!isEditable}
                            radioClass="iradio_square-blue local_iradio_square"
                            increaseArea="20%"
                            label=" <span class='label1'> Waiting </span>"
                            onClick={(e) => this.handleFormChange(e)}
                          />
                          <Radio
                            value={2}
                            name="approved"
                            disabled={!isEditable}
                            radioClass="iradio_square-blue local_iradio_square"
                            increaseArea="20%"
                            label=" <span class='label1'> Reject </span>"
                            onClick={(e) => this.handleFormChange(e)}
                          />
                        </RadioGroup>
                      </div>
                    </div>}
                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_day_week')}</label>
                      <div className="col-sm-5">
                        <select name="days_a_week" disabled={!isEditable} className="form-control" onChange={this.handleFormChange.bind(this)} value={formData.days_a_week} required>
                          <option>{translator.convert('add_recipe_choose_day')}</option>
                          {DAYS_PER_WEEK.map(item => <option key={`days_${item.value}`} value={item.value}>{item.title}</option>)}
                        </select>
                      </div>
                    </div>
                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_image')}</label>
                      <div className="col-sm-9 recipe-image-container">
                        <p className="text-aqua text-sm">Max 3 images allowed to upload for recipe. Image size should be greater than <b>800 X 1200 (width X height)</b></p>
                        {isEditable && (all_images.length + all_images_list.length) < 3 && <FileBase64
                          multiple={true}
                          max={3}
                          maxSize={10}
                          onDone={this.getFiles}
                        />}
                        {all_images && all_images.map((item, index) => this.getFilePath(item, index))}
                        {all_images_list.map((item, index) => this.getFilePath(item, index))}
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-md-12">
                <div className={`box ${theme.boxClass}`}>
                  <div className="box-header with-border">
                    <h3 className="box-title">Filters</h3>
                  </div>

                  <div className="box-body">
                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_select_meal')}</label>
                      <div className="col-sm-5">

                        {options.meals && <Select
                          value={formData.nutritionscheduleMeals}
                          isMulti={true}
                          name="nutritionscheduleMeals"
                          className="basic-multi-select"
                          classNamePrefix="select"
                          isDisabled={!isEditable}
                          onChange={(val) => this.handleMultiSelectChange(val, 'nutritionscheduleMeals')}
                          options={options.meals.map(item => {
                            return {
                              label: item.displayType,
                              value: item.instance_id,
                            };
                          })}
                        />}
                      </div>
                    </div>

                    <div className="form-group">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_my_mood')}</label>
                      <div className="col-sm-5">
                        {options.my_mood && <Select
                          value={formData.my_mood}
                          isMulti={true}
                          name="my_mood"
                          isDisabled={!isEditable}
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={(val) => this.handleMultiSelectChange(val, 'my_mood')}
                          options={options.my_mood.map(item => {
                            return {
                              label: item.name,
                              value: item.instance_id,
                            };
                          })}
                        />}
                      </div>
                    </div>


                    {/* <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_category')}</label>
                      <div className="col-sm-5">
                        {options.category && <Select
                          value={formData.category}
                          isMulti={false}
                          name="category"
                          isDisabled={!isEditable}
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={(val) => this.handleMultiSelectChange(val, 'category')}
                          options={options.category.map(item => {
                            return { label: item.name, value: item.instance_id };
                          })}
                        />}
                      </div>
                    </div> */}

                    <div className="form-group">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_category')}</label>
                      <div className="col-sm-5">
                        {options.filter && <Select
                          value={formData.recipesCategoriesId}
                          isMulti={true}
                          isDisabled={!isEditable}
                          name="recipesCategoriesId"
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={(val) => this.handleMultiSelectChange(val, 'recipesCategoriesId')}
                          options={options.filter.map(item => {
                            return {
                              label: item.name,
                              value: item.instance_id,
                            };
                          })}
                        />}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-md-12">
                <div className={`box ${theme.boxClass}`}>
                  <div className="box-header with-border">
                    <h3 className="box-title">Tags</h3>
                  </div>

                  <div className="box-body">
                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_food_preference')}</label>
                      <div className="col-sm-5">
                        {options.food_preferance && <Select
                          value={formData.food_preferance}
                          isMulti={true}
                          name="food_preferance"
                          isDisabled={!isEditable}
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={(val) => this.handleMultiSelectChange(val, 'food_preferance')}
                          options={options.food_preferance.map(item => {
                            return { label: item.name, value: item.instance_id };
                          })}
                        />}
                      </div>
                    </div>
                    <div className="form-group">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_season')}</label>
                      <div className="col-sm-5">

                        {options.season && <Select
                          value={formData.season}
                          isMulti={true}
                          name="season"
                          className="basic-multi-select"
                          classNamePrefix="select"
                          isDisabled={!isEditable}
                          onChange={(val) => this.handleMultiSelectChange(val, 'season')}
                          options={options.season.map(item => {
                            return {
                              label: item.name,
                              value: item.instance_id,
                            };
                          })}
                        />}
                      </div>
                    </div>

                    <div className="form-group">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_holidays')}</label>
                      <div className="col-sm-5">
                        {options.holidays && <Select
                          value={formData.holidays}
                          isMulti={true}
                          isDisabled={!isEditable}
                          name="holidays"
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={(val) => this.handleMultiSelectChange(val, 'holidays')}
                          options={options.holidays.map(item => {
                            return {
                              label: item.name,
                              value: item.instance_id,
                            };
                          })}
                        />}
                      </div>
                    </div>

                    <div className="form-group required">
                      <label className="col-sm-3 control-label">{translator.convert('add_recipe_allergens')}</label>
                      <div className="col-sm-5">
                        {options.allergens && <Select
                          value={formData.allergensIds}
                          isMulti={true}
                          isDisabled={!isEditable}
                          name="allergensIds"
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={(val) => this.handleMultiSelectChange(val, 'allergensIds')}
                          options={options.allergens.map(item => {
                            return {
                              label: item.name,
                              value: item.instance_id,
                            };
                          })}
                        />}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="box-footer">
              <Link className="btn btn-default" to="/manage/recipes">{translator.convert('cancel')}</Link>
              {isEditable && <button type="submit" className={`btn pull-right ${theme.btnClass}`}>{translator.convert('save')}</button>}
            </div>
          </form>
        </section>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    app: state.app,
    user: state.user,
    appConfig: state.common.appConfig,
    recipes: state.recipe,

  };
}

export default connect(mapStateToProps)(CreateRecipe);
