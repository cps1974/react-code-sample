// Core modules
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import treeChanges from 'tree-changes';
import { Table } from 'react-bootstrap';
import { Checkbox } from 'react-icheck';

// Configurations and actions
import { Themes, WEEKS, GENDERS } from 'constants/index';
import { API } from 'constants/api';
import { getRequest, postRequest, showAlert } from 'actions/index';
import Select from 'react-select';

// Components
// import Logo from 'components/Logo';
import Breadcrumb from 'components/Breadcrumb';
import translator from 'modules/translator';

export const intialState = {
    modalShow: false,
    permissionData: {},
    formData: {
        webrole_id: '',
        email: '',
        password: '',
        preposition: '',
        first_name: '',
        last_name: '',
        permission: [],
        username: '',
        active: 1,
        serie_id: '',
        lifestyle_id: '',
        active_subscription: 0,
        frequency: '',
        quote: '',
        location: '',
        height: '',
        bust: '',
        waist: '',
        hips: '',
        dob_day: '',
        dob_month: '',
        dob_year: '',
        mailable: 0,
        city: '',
        postal_code: '',
        housenumber: '',
        motivation: '',
        know_us_from: '',
        newsletter: 0,
        terms_of_use: '',
        latitude: '',
        longitude: '',
        weight: '',
        gender: null
    },
    info: {
        serie_id: '',
        lifestyle_id: '',
        active_subscription: 0,
        frequency: '',
        quote: '',
        location: '',
        height: '',
        bust: '',
        waist: '',
        hips: '',
        dob_day: '',
        dob_month: '',
        dob_year: '',
        mailable: 0,
        city: '',
        postal_code: '',
        housenumber: '',
        motivation: '',
        know_us_from: '',
        newsletter: 0,
        terms_of_use: '',
        latitude: '',
        longitude: '',
        weight: '',
        gender: null
    },
    algo: {
        body_type: { value: '', title: '' },
        eat_capacity: { value: '', title: '' },
        food_type: { value: '', title: '' },
        meal_time: { value: '', title: '' },
        muscles_fat: { value: '', title: '' },
        tee_option: { value: '', title: '' },
        kilo: ''
    },
    extra_options: {},
    updatedAlgo: {},
    gender: { label: '', value: '' }
};
const dateList = [];
const monthList = [];
const yearList = [];
for (let i = 1; i <= 31; i++) {
    dateList.push(i);
    if (i <= 12) {
        monthList.push(i);
    }
}
let currentDate = new Date();
let year = currentDate.getFullYear();
for (let i = 1951; i <= year; i++) {
    yearList.push(i);
}
let response = [];

export class CreateWebUsers extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = intialState;
        this.handleFormSave = this.handleFormSave.bind(this);
        this.handlePermissionChange = this.handlePermissionChange.bind(this);
    }

    static propTypes = {
        appConfig: PropTypes.object.isRequired,
        dispatch: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired,
        webUsers: PropTypes.object.isRequired,
    };

    componentWillMount() { }

    componentDidMount() {
        const { dispatch, match: { params } } = this.props;

        // const { payload, uri } = API.PERMISSION.LIST;
        // const { params: { user_id } } = match;
        // const user_id = params.user_id;

        // payload.user_type = 'core_users';
        // payload.user_id = user_id;
        // const options = {
        //     ...API.PERMISSION.LIST,
        //     payload,
        //     uri,
        // };
        if (params.role == 'others') dispatch(getRequest(API.OTHER_USER.OPTIONS));
        else dispatch(getRequest(API.WEB_USER.OPTIONS));

    }

    componentWillReceiveProps(nextProps) {
        console.log('nextProps', nextProps);
        const { params } = nextProps.match;
        const { webUsers } = nextProps;
        const { dispatch, history } = this.props;
        const { changedTo, changed } = treeChanges(this.props, nextProps);
        const isOthers = params.role == 'others';
        const { uri } = isOthers ? API.OTHER_USER.GET : API.WEB_USER.GET;


        // Reset user form after add new user.
        if (changedTo('webUsers.status', true)) {
            this.setState({ formData: intialState.formData });
        }

        // get user profile which need to edit after user form options API
        if (changedTo('webUsers.callback', 'GET') && params.user_id) {
            if (isOthers) dispatch(getRequest({ ...API.OTHER_USER.GET, uri: `${uri}/${params.user_id}` }));
            else dispatch(getRequest({ ...API.WEB_USER.GET, uri: `${uri}/${params.user_id}` }));
        }

        // update user profile which need to edit
        if (changedTo('webUsers.callback', 'redirect') && params.user_id) {
            history.push(`/manage/${params.core_user_id ? `${params.core_user_id}/` : ''}web/users${params.role == 'others' ? '/others' : ''}`);
        }

        // when webUser object updated
        if (webUsers && webUsers.webUser && webUsers.webUser.email && params.user_id) {
            let formData = {};
            let algo = {};
            let gender = {};
            let extra_options = {};
            if (isOthers) {
                formData = {
                    first_name: webUsers.webUser.firstname,
                    last_name: webUsers.webUser.lastname,
                    email: webUsers.webUser.email,
                    active: webUsers.webUser.active,
                    permission: webUsers.webUser.permission,
                    webrole_id: webUsers.webUser.role_id,
                    info: {},
                };
            }
            else {
                console.log('test', webUsers)
                formData.info = {};
                let except = ['password'];
                Object.keys(intialState.formData).map(k => { if (webUsers.webUser[k] && except.indexOf(k) === -1) formData[k] = webUsers.webUser[k]; });
                if (webUsers.webUser.info) {
                    Object.keys(intialState.info).map(k => { formData[k] = webUsers.webUser.info[k] ? webUsers.webUser.info[k] : intialState.info[k]; });
                }
                if (webUsers.webUser.info && webUsers.webUser.is_profile_created == 1) {
                    gender = {
                        label: webUsers.webUser.info.gender == 'M' ? 'Male' : 'Female', value: webUsers.webUser.info.gender
                    }
                } else {
                    gender = null
                }
                if (webUsers.webUser.algo && webUsers.webUser.is_profile_created == 1) {
                    algo = {
                        // body_type: { value: webUsers.webUser.algo.body.id, label: webUsers.webUser.algo.body.option_title },
                        // eat_capacity: { value: webUsers.webUser.algo.eatcapacity.id, label: webUsers.webUser.algo.eatcapacity.option_title },
                        // food_type: { value: webUsers.webUser.algo.foodtype.id, label: webUsers.webUser.algo.foodtype.option_title },
                        // meal_time: { value: webUsers.webUser.algo.meal_time, label: `${webUsers.webUser.algo.meal_time}x` },
                        // muscles_fat: { value: webUsers.webUser.algo.musclesfat.id, label: webUsers.webUser.algo.musclesfat.option_title },
                        // tee_option: { value: webUsers.webUser.algo.tee.option_title, label: webUsers.webUser.algo.tee.option_title },
                        // time: { value: webUsers.webUser.algo.time, label: webUsers.webUser.algo.time },
                        // kilo: webUsers.webUser.algo.kilo
                    };
                }
                if (webUsers.webUser.extra_options) {
                    extra_options = { ...webUsers.webUser.extra_options }
                }
            }
            formData.core_user_id = webUsers.webUser.core_user_id;
            console.log('formData', formData);
            this.setState({ formData, algo, extra_options, gender }, () => {
                console.log(this.state)
            });
            // this.setState({ permissionData:webUsers.options });
        }
        console.log('mukesh', intialState.permissionData);

    }

    handleFormChange = (event) => {
        const { formData, info } = this.state;
        const { target } = event;

        const value = target.type === 'checkbox' ? (target.checked ? 1 : 0) : target.value;
        const name = target.name;

        if (target.type === 'number') {
            if (value.length > 4)
                return false;
        }

        this.setState({
            formData: {
                ...formData,
                [name]: value,
            }
        });

        if (name == 'webrole_id' && value > 1) {
            this.setState({
                formData: {
                    ...formData,
                    ...info,
                    [name]: value,
                }
            });
        }
        // setTimeout(() => console.log('handle state', this.state.formData), 500);
    }

    handleFormSave = (event) => {
        event.preventDefault();
        const { props: { dispatch, user: { auth }, match: { params } }, state: { formData, updatedAlgo } } = this;
        let dobCtr = 0;
        if ((formData.dob_day > 0) && (formData.dob_month > 0) && (formData.dob_year > 0)) {
            dobCtr = 1;
        }
        if ((dobCtr == 0) && ((formData.dob_day > 0) || (formData.dob_month > 0) || (formData.dob_year > 0))) {
            dobCtr = 2;
        }
        if (dobCtr > 1) {
            return dispatch(showAlert('Date of birth should not be blank', { type: 'error' }));
        }

        if (params.role == 'others') {
            const payload = {
                role_id: formData.webrole_id,
                email: formData.email,
                password: formData.password,
                firstname: formData.first_name,
                lastname: formData.last_name,
                active: formData.active,
                permission: formData.permission.map(item => ({
                    can_add: item.can_add,
                    can_delete: item.can_delete,
                    can_view: item.can_view,
                    can_update: item.can_update,
                    can_all: item.can_all,
                    id: item.id,
                })),
            };
            if (!params.user_id) {
                // console.log("Create User", formData, payload)
                dispatch(postRequest({ ...API.OTHER_USER.ADD, payload }));
            }
            else {
                const { uri } = API.OTHER_USER.UPDATE;
                dispatch(postRequest({ ...API.OTHER_USER.UPDATE, payload, uri: `${uri}/${params.user_id}` }));
            }
        }
        else {
            let payload = {
                info: intialState.info,
            };
            Object.keys(formData).map((x) => {
                if (intialState.info.hasOwnProperty(x)) {
                    payload.info[x] = formData[x];
                }
                else {
                    payload[x] = formData[x];
                }
            });

            if (Object.keys(updatedAlgo).length > 0) {
                payload = {
                    ...payload,
                    algo: {
                        ...updatedAlgo
                    }
                }
            }

            if (!params.user_id) {
                payload.core_user_id = auth.user_id;
                if (params.role == 'model') payload.webrole_id = 1;
                // payload.permission = permissionData;
                console.log('Create User', payload)
                dispatch(postRequest({ ...API.WEB_USER.Add, payload }));
            }
            else {
                let { uri } = API.WEB_USER.UPDATE;
                console.log('new payloa', payload);
                dispatch(postRequest({ ...API.WEB_USER.UPDATE, payload, uri: `${uri}/${params.user_id}` }));
            }
        }


    }

    handlePermissionChange = (index, event) => {
        const { state: { formData }, props: { webUsers: { options } } } = this;
        const value = event.target.type === 'checkbox' ? (event.target.checked ? 1 : 0) : event.target.value;
        const name = event.target.name;
        console.log('handle checkbox', index, value, name);
        let newPermission = [];
        if (formData.permission.length) {
            formData.permission.map((item, i) => {
                if (i == index) newPermission.push({ ...item, [name]: value });
                else newPermission.push(item);
            });
        }
        else {
            options.permission.map((item, i) => {
                if (i == index) newPermission.push({ ...item, [name]: value });
                else newPermission.push(item);
            });
        }

        console.log('newPermission', newPermission);
        this.setState({
            formData: {
                ...formData,
                permission: newPermission,
            }
        });
    }

    loadPermission = (item, index) => {
        const { formData } = this.state;
        const { user: { auth } } = this.props;
        const key = auth.core_user_roldid == 1 ? 'for_admin' : 'for_subadmin';
        // console.log(item);
        return item[key] ? (
            <tr key={`permission_${index}`}>
                {/* <td>{index + 1}</td> */}
                <td>{item.display_name ? item.display_name : 'N/A'}</td>

                {!item.is_all && <td>

                    <Checkbox
                        checkboxClass="icheckbox_square-blue"
                        increaseArea="20%"
                        label=" &nbsp;"
                        value="1"
                        name="can_view"
                        checked={formData.permission && formData.permission.length && formData.permission[index].can_view == 1 ? 1 : 0}
                        onChange={(e) => this.handlePermissionChange(index, e)}
                    />
                </td>}

                {!item.is_all && <td>
                    <Checkbox
                        checkboxClass="icheckbox_square-blue"
                        increaseArea="20%"
                        label=" &nbsp;"
                        value="1"
                        name="can_add"
                        checked={formData.permission && formData.permission.length && formData.permission[index].can_add == 1 ? 1 : 0}
                        onChange={(e) => this.handlePermissionChange(index, e)}
                    />
                </td>}

                {!item.is_all && <td>
                    <Checkbox
                        checkboxClass="icheckbox_square-blue"
                        increaseArea="20%"
                        label=" &nbsp;"
                        value="1"
                        name="can_update"
                        checked={formData.permission && formData.permission.length && formData.permission[index].can_update == 1 ? 1 : 0}
                        onChange={(e) => this.handlePermissionChange(index, e)}
                    />
                </td>}

                {!item.is_all && <td>
                    <Checkbox
                        checkboxClass="icheckbox_square-blue"
                        increaseArea="20%"
                        label=" &nbsp;"
                        value="1"
                        name="can_delete"
                        checked={formData.permission && formData.permission.length && formData.permission[index].can_delete == 1 ? 1 : 0}
                        onChange={(e) => this.handlePermissionChange(index, e)}
                    />

                </td>}
                {item.is_all ? <td colSpan="4">
                    <Checkbox
                        checkboxClass="icheckbox_square-blue"
                        increaseArea="20%"
                        label=" &nbsp;"
                        value="1"
                        name="can_all"
                        checked={formData.permission && formData.permission.length && formData.permission[index].can_all == 1 ? 1 : 0}
                        onChange={(e) => this.handlePermissionChange(index, e)}
                    />

                </td> : ''}
            </tr>
        ) : '';
    }

    handleMultiSelectChange = (value, key) => {
        console.log(value)
        const { algo, updatedAlgo, formData } = this.state;
        const { match } = this.props
        if (key == 'gender') {
            this.setState({
                formData: {
                    ...formData,
                    [key]: value.value,
                },
                gender: value
            }, () => {
                console.log('this.state', this.state)
            });
        } else if (key == 'kilo') {
            this.setState({
                algo: {
                    ...algo,
                    [key]: value,
                },
                updatedAlgo: {
                    ...updatedAlgo,
                    [key]: value
                }
            }, () => {
                console.log('this.state', this.state)
            });
        } else {
            this.setState({
                algo: {
                    ...algo,
                    [key]: value,
                },
                updatedAlgo: {
                    ...updatedAlgo,
                    [key]: value.value
                }
            }, () => {
                console.log('this.state', this.state)
            });
        }


    }

    render() {
        const { appConfig, webUsers: { options, isLoading }, match: { params } } = this.props;
        const { formData, extra_options, algo, gender } = this.state;
        const { theme } = appConfig;

        return (
            // <!-- Content Wrapper. Contains page content -->
            <div key="create-web-user" className="content-wrapper">
                {isLoading && <div className="full-screen-loading"></div>}
                <Breadcrumb title={params.user_id ? translator.convert('Update') : translator.convert('add_member')} />
                {/* <!-- Main content --> */}
                <section className="content">
                    <form className="form-horizontal" onSubmit={this.handleFormSave}>
                        <div className="row">
                            <div className="col-md-12">
                                <div className={`box ${theme.boxClass}`}>
                                    <div className="box-header with-border">
                                        <h3 className="box-title">{translator.convert('account_information')}</h3>
                                    </div>

                                    <div className="box-body">

                                        {params.role == 'others' && <div className="form-group required">
                                            <label className="col-sm-2 control-label">{translator.convert('role')}</label>
                                            <div className="col-sm-6">
                                                <select name="webrole_id" onChange={this.handleFormChange.bind(this)} value={formData.webrole_id} className="form-control" required>
                                                    <option value="">{translator.convert('select_role')}</option>
                                                    {options.role && options.role.map(item => {
                                                        return item.role_name != 'Model' && <option key={Math.random()} value={item.role_id}>{item.role_name}</option>
                                                    }
                                                    )}
                                                </select>
                                            </div>
                                        </div>}

                                        <div className="form-group required">
                                            <label className="col-sm-2 control-label">{translator.convert('email')}</label>
                                            <div className="col-sm-6">
                                                <input type="email" name="email" onChange={this.handleFormChange.bind(this)} value={formData.email} className="form-control" required />
                                            </div>
                                        </div>

                                        <div className="form-group required">
                                            <label className="col-sm-2 control-label">{translator.convert('first_name')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="first_name" onChange={this.handleFormChange.bind(this)} value={formData.first_name} className="form-control" required />
                                            </div>
                                        </div>
                                        {/* <div className="form-group required">
                                            <label className="col-sm-2 control-label">{translator.convert('insertion')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="preposition" onChange={this.handleFormChange.bind(this)} value={formData.preposition} className="form-control" required />
                                            </div>
                                        </div> */}
                                        <div className="form-group required">
                                            <label className="col-sm-2 control-label">{translator.convert('last_name')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="last_name" onChange={this.handleFormChange.bind(this)} value={formData.last_name} className="form-control" required />
                                            </div>
                                        </div>

                                        {/* <div className="form-group required">
                                            <label className="col-sm-2 control-label">{translator.convert('user_name')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="username" onChange={this.handleFormChange.bind(this)} value={formData.username} className="form-control" required />
                                            </div>
                                        </div> */}

                                        <div className={`form-group ${params.user_id ? '' : 'required'}`}>
                                            <label className="col-sm-2 control-label">{translator.convert('password')}</label>
                                            <div className="col-sm-6">
                                                <input type="password" name="password" onChange={this.handleFormChange.bind(this)} value={formData.password} className="form-control" required={params.user_id ? false : true} />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('active')}</label>
                                            <div className="col-sm-6">
                                                {/* <input type="checkbox" name="active" onChange={this.handleFormChange.bind(this)} checked={formData.active == 1} /> */}
                                                <Checkbox
                                                    checkboxClass="icheckbox_square-blue"
                                                    increaseArea="20%"
                                                    label=" &nbsp;"
                                                    value="1"
                                                    name="active"
                                                    checked={formData.active == 1}
                                                    onChange={(e) => this.handleFormChange(e)}
                                                />
                                            </div>
                                        </div>

                                        <div className="col-sm-12 user-permission">
                                            <div className="col-sm-1"> </div>
                                            <div className="col-sm-10">


                                            </div>

                                        </div>



                                    </div>

                                </div>
                            </div>
                        </div>

                        {params.role == 'model' && <div className="row">
                            <div className="col-md-12">
                                <div className={`box ${theme.boxClass}`}>
                                    <div className="box-header with-border">
                                        <h3 className="box-title">{translator.convert('lifestyle_abonnement')}</h3>
                                    </div>

                                    <div className="box-body">

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('serie')}</label>
                                            <div className="col-sm-6">
                                                <select name="serie_id" value={formData.serie_id} onChange={this.handleFormChange.bind(this)} className="form-control">
                                                    <option>{translator.convert('not_yet_made_choise')}</option>
                                                    {options.series && options.series.map(item => <option key={Math.random()} value={item.instance_id}>{item.title}</option>)}
                                                </select>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('lifestyle')}</label>
                                            <div className="col-sm-6">
                                                <select name="lifestyle_id" value={formData.lifestyle_id} onChange={this.handleFormChange.bind(this)} className="form-control">
                                                    <option>{translator.convert('not_yet_made_choise')}</option>
                                                    {options.lifestyle && options.lifestyle.map(item => <option key={Math.random()} value={item.instance_id}>{item.title}</option>)}
                                                </select>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('active_subscription')}</label>
                                            <div className="col-sm-6">
                                                {/* <input type="checkbox" name="active_subscription" onChange={this.handleFormChange.bind(this)} checked={formData.active_subscription == 1} /> */}
                                                <Checkbox
                                                    checkboxClass="icheckbox_square-blue"
                                                    increaseArea="20%"
                                                    label=" &nbsp;"
                                                    value="1"
                                                    name="active_subscription"
                                                    checked={formData.active_subscription == 1}
                                                    onChange={(e) => this.handleFormChange(e)}
                                                />
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>}

                        {params.role == 'model' && <div className="row">
                            <div className="col-md-12">
                                <div className={`box ${theme.boxClass}`}>
                                    <div className="box-header with-border">
                                        <h3 className="box-title">Remaining</h3>
                                    </div>

                                    <div className="box-body">

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('sport_frequency')}</label>
                                            <div className="col-sm-6">
                                                <select name="frequency" value={formData.frequency} onChange={this.handleFormChange.bind(this)} className="form-control">
                                                    <option>Choose sport frequency</option>
                                                    <option value="1 to 3 times per week">1 {translator.convert('tot')} 3  {translator.convert('times_per_week')}</option>
                                                    <option value="3 to 5 times per week">3 {translator.convert('tot')} 5  {translator.convert('times_per_week')}</option>
                                                    <option value="5 tot 7 times per week">5 {translator.convert('tot')} 7  {translator.convert('times_per_week')}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('quote')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="quote" value={formData.quote} onChange={this.handleFormChange.bind(this)} className="form-control" />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('current_location')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="location" value={formData.location} onChange={this.handleFormChange.bind(this)} className="form-control" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>}

                        {params.role == 'model' && <div className="row">
                            <div className="col-md-12">
                                <div className={`box ${theme.boxClass}`}>
                                    <div className="box-header with-border">
                                        <h3 className="box-title">{translator.convert('Body details')}</h3>
                                    </div>
                                    <div className="box-body">

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('height')}</label>
                                            <div className="col-sm-3 col-md-2">
                                                <input type="number" maxLength={4} name="height" value={formData.height}
                                                    onChange={(e) => {

                                                        this.handleFormChange(e);


                                                    }}
                                                    className="form-control" />
                                            </div>
                                            <div className="col-sm-1" style={{ marginLeft: -25 }}>
                                                cm
                                            </div>

                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('weight')}</label>
                                            <div className="col-sm-3 col-md-2">
                                                <input type="number" name="weight" value={formData.weight}
                                                    onChange={(e) => {
                                                        this.handleFormChange(e);
                                                    }} className="form-control" />
                                            </div>
                                            <div className="col-sm-1" style={{ marginLeft: -25 }}>
                                                kg
                                            </div>
                                        </div>

                                        {/* <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('bust')}</label>
                                            <div className="col-sm-3 col-md-2">
                                                <input type="number" name="bust" value={formData.bust}
                                                    onChange={(e) => {
                                                        this.handleFormChange(e);
                                                    }} className="form-control" />
                                            </div>
                                            <div className="col-sm-1" style={{ marginLeft: -25 }}>
                                                cm
                                            </div>
                                        </div> */}

                                        {/* <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('waist')}</label>
                                            <div className="col-sm-3 col-md-2">
                                                <input type="number" name="waist" value={formData.waist}
                                                    onChange={(e) => {
                                                        this.handleFormChange(e);
                                                    }} className="form-control" />
                                            </div>
                                            <div className="col-sm-1" style={{ marginLeft: -25 }}>
                                                cm
                                            </div>
                                        </div> */}

                                        {/* <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('hips')}</label>
                                            <div className="col-sm-3 col-md-2">
                                                <input type="number" name="hips" value={formData.hips}
                                                    onChange={(e) => {
                                                        this.handleFormChange(e);
                                                    }} className="form-control" />
                                            </div>
                                            <div className="col-sm-1" style={{ marginLeft: -25 }}>
                                                cm
                                            </div>
                                        </div> */}
                                    </div>
                                </div>
                            </div>
                        </div>}

                        {params.role == 'model' && <div className="row">
                            <div className="col-md-12">
                                <div className={`box ${theme.boxClass}`}>
                                    <div className="box-header with-border">
                                        <h3 className="box-title">{translator.convert('date_of_birth')}</h3>
                                    </div>

                                    <div className="box-body">

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('birthday')}</label>
                                            <div className="col-sm-3 col-md-2">
                                                <select name="dob_day" value={formData.dob_day}
                                                    onChange={this.handleFormChange.bind(this)} className="form-control">
                                                    <option>{translator.convert('day')}</option>
                                                    {dateList.map((element, key) => (
                                                        <option key={key} value={element}>{element}</option>
                                                    ))}
                                                </select>
                                            </div>
                                            <div className="col-sm-3 col-md-2">
                                                <select name="dob_month" value={formData.dob_month}
                                                    onChange={this.handleFormChange.bind(this)} className="form-control">
                                                    <option>{translator.convert('month')}</option>
                                                    {monthList.map((element, key) => (
                                                        <option key={key} value={element}>{element}</option>
                                                    ))}
                                                </select>
                                            </div>
                                            <div className="col-sm-3 col-md-3">
                                                <select name="dob_year" value={formData.dob_year}
                                                    onChange={this.handleFormChange.bind(this)} className="form-control">
                                                    <option>{translator.convert('year')}</option>
                                                    {yearList.map((element, key) => (
                                                        <option key={key} value={element}>{element}</option>
                                                    ))}
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>}

                        {params.role == 'model' && <div className="row">
                            <div className="col-md-12">
                                <div className={`box ${theme.boxClass}`}>
                                    <div className="box-header with-border">
                                        <h3 className="box-title">{translator.convert('address_data')}</h3>
                                    </div>

                                    <div className="box-body">

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('city')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="city" onChange={this.handleFormChange.bind(this)} value={formData.city} className="form-control" />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('postcode')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="postal_code" onChange={this.handleFormChange.bind(this)} value={formData.postal_code} className="form-control" />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('house_number')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="housenumber" onChange={this.handleFormChange.bind(this)} value={formData.housenumber} className="form-control" />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('mailable')}</label>
                                            <div className="col-sm-6">
                                                {/* <input type="checkbox" name="mailable" onChange={this.handleFormChange.bind(this)} checked={formData.mailable == 1} /> */}
                                                <Checkbox
                                                    checkboxClass="icheckbox_square-blue"
                                                    increaseArea="20%"
                                                    label=" &nbsp;"
                                                    value="1"
                                                    name="mailable"
                                                    checked={formData.mailable == 1}
                                                    onChange={(e) => this.handleFormChange(e)}
                                                />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>}

                        {/* {params.role == 'model' && params.user_id && <div className="row">
                            <div className="col-md-12">
                                <div className={`box ${theme.boxClass}`}>
                                    <div className="box-header with-border">
                                        <h3 className="box-title">{translator.convert('On boarding questions')}</h3>
                                    </div>

                                    <div className="box-body">
                                         <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('Body type')}</label>
                                            <div className="col-sm-6">
                                                {Object.keys(extra_options).length > 0 && extra_options.body_type && <Select
                                                    value={algo.body_type}
                                                    isMulti={false}
                                                    required
                                                    name="productgroep_id"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'body_type')}
                                                    options={extra_options.body_type.map(item => {
                                                        return { label: item.option_title, value: item.id };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('Eat capacity')}</label>
                                            <div className="col-sm-6">
                                                {Object.keys(extra_options).length > 0 && extra_options.eat_capacity && <Select
                                                    value={algo.eat_capacity}
                                                    isMulti={false}
                                                    required
                                                    name="productgroep_id"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'eat_capacity')}
                                                    options={extra_options.eat_capacity.map(item => {
                                                        return { label: item.option_title, value: item.id };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('Food type')}</label>
                                            <div className="col-sm-6">
                                                {Object.keys(extra_options).length > 0 && extra_options.food_type && <Select
                                                    value={algo.food_type}
                                                    isMulti={false}
                                                    required
                                                    name="productgroep_id"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'food_type')}
                                                    options={extra_options.food_type.map(item => {
                                                        return { label: item.option_title, value: item.id };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('Meal time')}</label>
                                            <div className="col-sm-6">
                                                {Object.keys(extra_options).length > 0 && extra_options.meal_time && <Select
                                                    value={algo.meal_time}
                                                    isMulti={false}
                                                    required
                                                    name="productgroep_id"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'meal_time')}
                                                    options={extra_options.meal_time.map(item => {
                                                        return { label: item.option_title, value: item.id };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('Muscle fat')}</label>
                                            <div className="col-sm-6">
                                                {Object.keys(extra_options).length > 0 && extra_options.muscles_fat && <Select
                                                    value={algo.muscles_fat}
                                                    isMulti={false}
                                                    required
                                                    name="productgroep_id"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'muscles_fat')}
                                                    options={extra_options.muscles_fat.map(item => {
                                                        return { label: item.option_title, value: item.id };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('Tee option')}</label>
                                            <div className="col-sm-6">
                                                {Object.keys(extra_options).length > 0 && extra_options.tee_option && <Select
                                                    value={algo.tee_option}
                                                    isMulti={false}
                                                    required
                                                    name="productgroep_id"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'tee_option')}
                                                    options={extra_options.tee_option.map(item => {
                                                        return { label: item.option_title, value: item.id };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('Time period')}</label>
                                            <div className="col-sm-6">
                                                {WEEKS && <Select
                                                    value={algo.time}
                                                    isMulti={false}
                                                    required
                                                    name="productgroep_id"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'time')}
                                                    options={WEEKS.map(item => {
                                                        return { label: item.label, value: item.value };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('Gender')}</label>
                                            <div className="col-sm-6">
                                                {GENDERS && <Select
                                                    value={gender}
                                                    isMulti={false}
                                                    required
                                                    name="productgroep_id"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'gender')}
                                                    options={GENDERS.map(item => {
                                                        console.log(item)
                                                        return { label: item.label, value: item.value };
                                                    })}
                                                />}
                                            </div>
                                        </div> 

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('Kilo')}</label>
                                            <div className="col-sm-3 col-md-2">
                                                <input type="number" maxLength={4} name="kilo" value={algo.kilo}
                                                    onChange={(e) => {
                                                        this.handleMultiSelectChange(e.target.value, 'kilo')
                                                    }}
                                                    className="form-control" />
                                            </div>
                                            <div className="col-sm-1" style={{ marginLeft: -25 }}>
                                                kg
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>} */}

                        {params.role == 'model' && <div className="row">
                            <div className="col-md-12">
                                <div className={`box ${theme.boxClass}`}>
                                    <div className="box-header with-border">
                                        <h3 className="box-title">{translator.convert('others')}</h3>
                                    </div>
                                    <div className="box-body">

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('motivation')}</label>
                                            <div className="col-sm-6">
                                                <textarea type="text" name="motivation" onChange={this.handleFormChange.bind(this)} value={formData.motivation} className="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('we_know_from')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="know_us_from" onChange={this.handleFormChange.bind(this)} value={formData.know_us_from} className="form-control" />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('newsletter')}</label>
                                            <div className="col-sm-6">
                                                {/* <input type="checkbox" name="newsletter" onChange={this.handleFormChange.bind(this)} checked={formData.newsletter == 1} /> */}
                                                <Checkbox
                                                    checkboxClass="icheckbox_square-blue"
                                                    increaseArea="20%"
                                                    label=" &nbsp;"
                                                    value="1"
                                                    name="newsletter"
                                                    checked={formData.newsletter == 1}
                                                    onChange={(e) => this.handleFormChange(e)}
                                                />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-2 control-label">{translator.convert('terms_conditions')}</label>
                                            <div className="col-sm-6">
                                                <input type="text" name="terms_of_use" onChange={this.handleFormChange.bind(this)} value={formData.terms_of_use} className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>}

                        {params.role == 'others' && <div className="row">
                            <div className="col-md-12">
                                <div className={`box ${theme.boxClass}`}>
                                    <div className="box-header with-border">
                                        <h3 className="box-title" >{translator.convert('user_permission_title')}</h3>
                                    </div>

                                    <div className="box-body">

                                        <Table responsive hover bordered>
                                            <thead>
                                                <tr>
                                                    {/* <th>{translator.convert('ID')}</th> */}
                                                    <th>{translator.convert('permission_role_name')}</th>
                                                    <th>{translator.convert('permission_view')}</th>

                                                    <th>{translator.convert('permission_add')}</th>
                                                    <th>{translator.convert('permission_update')}</th>
                                                    <th>{translator.convert('permission_delete')}</th>
                                                    {/* <th>{translator.convert('All Permission')}</th> */}


                                                </tr>
                                            </thead>
                                            <tbody>
                                                {options.permission && options.permission.map((item, index) => this.loadPermission(item, index))}

                                            </tbody>
                                        </Table>
                                    </div>

                                </div>
                            </div>
                        </div>}



                        <div className="box-footer">
                            <Link className="btn btn-default" to={`/manage/${params.core_user_id ? `${params.core_user_id}/` : ''}web/users${params.role == 'others' ? '/others' : ''}`}>{translator.convert('cancel')}</Link>
                            <button type="submit" className={`btn pull-right ${theme.btnClass}`}>{translator.convert('save')}</button>
                        </div>
                    </form>
                </section>
            </div>
        );
    }
}

/* istanbul ignore next */
function mapStateToProps(state) {
    return {
        user: state.user,
        appConfig: state.common.appConfig,
        webUsers: state.common.webUsers,
        //  permission: state.common.permission,
    };
}

export default connect(mapStateToProps)(CreateWebUsers);
