// Core modules
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import treeChanges from 'tree-changes';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Radio, RadioGroup } from 'react-icheck';
import Select from 'react-select';

// Configurations and actions
import config from 'config';
import { API } from 'constants/api';
import { getRequest, postRequest, showAlert, commonLocalUpdate } from 'actions/index';
import images from 'images';
import { Themes, DAYS_PER_WEEK, MEDIA_URL, MEDIA_DIR, S3_MEDIA_URL, S3_UPLOAD_CONFIG } from 'constants/index';

// Components
// import Logo from 'components/Logo';
import Breadcrumb from 'components/Breadcrumb';
import translator from 'modules/translator';
import { displayApproved, rolePermission, getImageName } from 'modules/helpers';
import { INTENSITY } from 'constants/index';
import S3FileUpload from 'components/react-s3';
import FileBase64 from 'components/FileBase64';
import PreLoader from 'components/PreLoader';

export const intialState = {
    modalShow: false,
    formData: {
        approved: '',
        // is_pro: 0,
        // lifestyle_show: 0,
        // free_show: 0,
        workoutpackage_id: 0,
        // instance_order: 99999,
        preview_image: '',
        title: '',
        tags: '',
        content: '',
        duration: '',
        active: 1,
        exercises: [],
        muscles: [],
        video_focus: [],
        video_label: '',
        intensity: '',
        video_vimeo: '',
        video_categories_id: [],
        cast: [''],
        image: '',
    },
    imageFile: '',
    s3loader: false,
};

export class CreateVideo extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = intialState;
        this.handleFormSave = this.handleFormSave.bind(this);
        this.editorConfig = {
            toolbar: ['Heading', '|', 'Bold', 'Italic', 'Alignment', 'bulletedList', 'numberedList', 'Link', 'BlockQuote', 'Undo', 'Redo'],
        }
    }

    static propTypes = {
        appConfig: PropTypes.object.isRequired,
        dispatch: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired,
        videos: PropTypes.object.isRequired,
    };

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(getRequest(API.VIDEOS.OPTIONS));
        console.log('this.props', this.props);
    }

    componentWillReceiveProps(nextProps) {
        // console.log("nextProps", nextProps);
        const { params } = nextProps.match;
        const { videos } = nextProps;
        const { dispatch, history } = this.props;
        const { changedTo, changed } = treeChanges(this.props, nextProps);
        const { uri } = API.VIDEOS.GET;


        // Reset video form after add new video. 
        if (changedTo('videos.status', true) && changedTo('videos.callback', 'success')) {
            this.tagData = '';
            this.contentData = '';
            this.setState({ formData: intialState.formData });
        }

        // get user profile which need to edit after user form options API
        if (changedTo('videos.callback', 'GET') && params.instance_id) {
            dispatch(getRequest({ ...API.VIDEOS.GET, uri: `${uri}/${params.instance_id}` }));

        }

        // update user profile which need to edit
        if (changedTo('videos.callback', 'redirect') && params.instance_id) {
            history.push('/manage/workout/videos');
        }

        if (changedTo('videos.callback', 'success')) {
            history.push('/manage/workout/videos');
        }

        if (changed('videos.videoInfo.instance_id') && params.instance_id) {
            dispatch(postRequest({ ...API.VIDEOS.VARIFY, payload: { code: videos.videoInfo.video_vimeo } }));
            let formData = {};
            let except = ['exercises', 'muscles', 'tag', 'content', 'video_focus', 'video_label', 'cast'];
            Object.keys(intialState.formData).map(k => { if (videos.videoInfo[k] && except.indexOf(k) === -1) formData[k] = videos.videoInfo[k]; });
            formData.core_user_id = videos.videoInfo.core_user_id;
            this.tagData = videos.videoInfo.tags;
            this.contentData = videos.videoInfo.content;
            formData.muscles = videos.videoInfo.muscles.map(item => {
                return { label: item.title ? item.title : 'N/A', value: item.muscle_id };
            });
            formData.exercises = videos.videoInfo.exercises.map(item => {
                return { label: item.title ? item.title : 'N/A', value: item.exercise_id };
            });
            formData.video_focus = videos.videoInfo.video_focus.map(item => {
                return { label: item.title ? item.title : 'N/A', value: item.focus_id };
            });
            formData.video_label = videos.videoInfo.video_label == null ? [] : { label: videos.videoInfo.video_label.name, value: videos.videoInfo.video_label.id };
            formData.video_categories_id = videos.videoInfo.video_categories_id == null ? [] : { label: videos.videoInfo.video_categories_id.title, value: videos.videoInfo.video_categories_id.instance_id };
            formData.intensity = videos.videoInfo.intensity == null ? [] : { label: videos.videoInfo.intensity, value: videos.videoInfo.intensity };
            formData.cast = videos.videoInfo.cast.length == 0 ? [''] : videos.videoInfo.cast.map(item => {
                return item.text
            });
            formData.image = videos.videoInfo.image
            formData.active = videos.videoInfo.active
            this.setState({ formData });
        }

    }

    componentWillUnmount() {
        const { dispatch } = this.props;
        dispatch(commonLocalUpdate({ root: 'videos', key: 'videoInfo', data: {}}));
    }

    onVarifyVimeoCode = (event) => {
        console.log(event.target.value);
        const { value } = event.target;
        // const { payload } = API.VIDEOS.VARIFY;
        const { dispatch } = this.props;
        dispatch(postRequest({ ...API.VIDEOS.VARIFY, payload: { code: value } }));
    }

    handleFormChange = (event) => {
        const { formData } = this.state;
        const { target } = event;
        const { videos } = this.props;

        const value = target.type === 'checkbox' ? (target.checked ? 1 : 0) : target.value;
        const name = target.name;

        if (name == 'workoutpackage_id') {
            let categories_id = videos.options.workoutpackages.filter(item => item.instance_id == value);
            console.log(categories_id);
            if (categories_id.length) {
                this.setState({ formData: { ...formData, [name]: value, video_categories_id: categories_id[0].workoutpackagescategorie_id } });
            }
        }
        else {
            this.setState({
                formData: {
                    ...formData,
                    [name]: value,
                },
            });
        }
    }

    handleMultiSelectChange = (value, key) => {
        const { formData } = this.state;
        this.setState({
            formData: {
                ...formData,
                [key]: value,
            },
        });
    }

    addRange = (e) => {
        const { formData } = this.state;
        const range = formData.cast.concat(['']);
        this.setState({
            formData: {
                ...formData,
                cast: range,
            },
        });
    };

    removeRange = (i) => {
        const { formData } = this.state;
        const { dispatch } = this.props;
        let tempArray = { ...formData.cast }
        const removeItem = formData.cast[i];
        const range = formData.cast.filter((item, index) => {
            return index != i
        });
        console.log(range, 'range')
        // if (removeItem.id) {
        //     tempArray.splice(removeItem.id, 1);
        // }
        this.setState({
            formData: {
                ...formData,
                cast: range,
            },
        });

    }

    handleRangeChange = (event, index) => {
        const { target } = event;
        const { formData } = this.state;
        console.log(target.validity.valid, event);
        if (!target.validity.valid && target.value != '') { return; }
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let range = [];
        formData.cast.find((item, i) => {
            if (i == index) range.push(value);
            else range.push(item);
        });
        this.setState({
            formData: {
                ...formData,
                cast: range,
            },
        });
    }

    getFiles = files => {
        console.log('files', files);
        const { dispatch, recipes } = this.props;
        const { formData } = this.state;
        if (files.success) {
            this.setState({
                imageFile: files,
            }, () => {
                console.log(this.state)
            });
        }
        else {
            dispatch(showAlert(files.message, { type: 'error' }));
        }

    }

    handleFormSave = (event) => {
        event.preventDefault();
        const { props: { dispatch, user: { auth }, videos, match: { params } }, state: { formData, imageFile } } = this;
        console.log('handle add member', formData, this.contentData);
        const exercises = formData.exercises.map(item => { return item.value; });
        const muscles = formData.muscles.map(item => { return item.value; });
        const video_label = formData.video_label.value ? formData.video_label.value : '';
        const categories_id = formData.video_categories_id.value ? formData.video_categories_id.value : '';
        const intensity = formData.intensity.value ? formData.intensity.value : '';
        let video_focus_final = [];
        if (formData.video_focus.length > 0) {
            formData.video_focus.map(item => { video_focus_final.push(item.value) })
        }
        const video_focus = formData.video_focus.map(item => { return item.value; })
        console.log(categories_id, 'test')
        if (!muscles.length) return dispatch(showAlert('Health Muscles group is mandatory', { type: 'error' }));
        if (categories_id == '') return dispatch(showAlert('Video series is mandatory', { type: 'error' }));
        if (!exercises.length) return dispatch(showAlert('Link to excercise is mandatory', { type: 'error' }));
        // if (!params.instance_id && formData.image == '' && imageFile == '') return dispatch(showAlert('Workout image is mandatory', { type: 'error' }));
        if (!videos.vimeoInfo.thumbnail_url && !formData.image && !imageFile) { return dispatch(showAlert('Thumb image is required!', { type: 'error' })); }
        const payload = {
            core_user_id: params.instance_id ? formData.core_user_id : auth.user_id,
            // is_pro: formData.is_pro,
            // lifestyle_show: formData.lifestyle_show,
            // free_show: formData.free_show,
            // workoutpackage_id: formData.workoutpackage_id,
            // instance_order: formData.instance_order,
            preview_image: videos.vimeoInfo.thumbnail_url,
            title: formData.title,
            tags: this.tagData,
            content: this.contentData,
            duration: videos.vimeoInfo.duration,
            approved: formData.approved,
            active: formData.active,
            exercises,
            muscles,
            video_focus: video_focus_final,
            cast: formData.cast,
            video_label,
            intensity,
            video_vimeo: formData.video_vimeo,
            video_categories_id: categories_id,
            image: formData.image,

        };
        if (imageFile != '') {
            this.setState({ s3loader: true })
            this._s3FileUpload(imageFile, payload);
        }
        else {
            this.saveWorkoutVideo(payload);
        }
    }

    s3fileNames = ''

    _s3FileUpload = (file, payload) => {
        const newName = getImageName(file.name);
        const s3Config = { ...S3_UPLOAD_CONFIG, dirName: `${S3_UPLOAD_CONFIG.dirName}/${MEDIA_DIR.WORKOUT}` };
        S3FileUpload
            .uploadFile(file.file, s3Config, { newName })
            .then(data => {
                console.log(data, 'afters3', newName);
                // const { result } = data;
                this.s3fileNames = newName;
                this.saveWorkoutVideo(payload);
            })
            .catch(err => {
                this.setState({
                    s3loader: false,
                });
                console.error(err);
            });
    }

    saveWorkoutVideo = (payloadToSend) => {
        this.setState({ s3loader: false });
        const { dispatch, match: { params } } = this.props;
        const { formData } = this.state;
        const payload = { ...payloadToSend, image: this.s3fileNames };
        console.log('handle add video', payload);
        if (params.instance_id) {
            const { uri } = API.VIDEOS.UPDATE;
            dispatch(postRequest({ ...API.VIDEOS.UPDATE, payload, uri: `${uri}/${params.instance_id}` }));
        }
        else {
            dispatch(postRequest({ ...API.VIDEOS.ADD, payload }));
        }
    }

    render() {
        const { user: { auth }, appConfig, videos: { options, isLoading, vimeoInfo, smallLoader, videoInfo }, match: { params } } = this.props;
        const { formData, s3loader, imageFile } = this.state;
        const { theme } = appConfig;
        console.log('formData', formData);
        return (
            // <!-- Content Wrapper. Contains page content -->
            <div key="home" className="content-wrapper">
                {(s3loader || (isLoading && !smallLoader)) && <div className="full-screen-loading"></div>}
                <Breadcrumb title="Add Video" />
                {/* <!-- Main content --> */}
                <section className="content">
                    <form className="form-horizontal" onSubmit={this.handleFormSave}>
                        <div className="row">
                            <div className="col-md-12">
                                <div className={`box ${theme.boxClass}`}>
                                    <div className="box-header with-border">
                                        <h3 className="box-title">General info</h3>
                                    </div>

                                    <div className="box-body">
                                        {/* <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('only_for_pro')}</label>
                                            <div className="col-sm-6">
                                                <input type="checkbox" name="is_pro" onChange={this.handleFormChange.bind(this)} checked={formData.is_pro == 1} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('show_at_lifestyles')}</label>
                                            <div className="col-sm-6">
                                                <input type="checkbox" name="lifestyle_show" onChange={this.handleFormChange.bind(this)} checked={formData.lifestyle_show == 1} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('show_when_exercising_free')}</label>
                                            <div className="col-sm-5">
                                                <input type="checkbox" name="free_show" onChange={this.handleFormChange.bind(this)} checked={formData.free_show == 1} />
                                            </div>
                                        </div> */}
                                        <div className="form-group required">
                                            <label className="col-sm-3 control-label">{translator.convert('Spiergroep')}</label>
                                            <div className="col-sm-5">
                                                {/* <select name="muscles" onChange={this.handleFormChange.bind(this)} value={formData.muscles} className="form-control" required>
                                                    <option value="">You belongs to</option>
                                                    {options.muscles && options.muscles.map(item => <option key={Math.random()} value={item.instance_id}>{item.title}</option>)}
                                                </select> */}
                                                {options.muscles && <Select
                                                    value={formData.muscles}
                                                    isMulti={true}
                                                    name="muscles"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'muscles')}
                                                    options={options.muscles.map(item => {
                                                        return { label: item.title, value: item.instance_id };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('Video Focus')}</label>
                                            <div className="col-sm-5">
                                                {options.video_focus && <Select
                                                    value={formData.video_focus}
                                                    isMulti={true}
                                                    name="video_focus"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'video_focus')}
                                                    options={options.video_focus.map(item => {
                                                        return { label: item.name, value: item.id };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group required">
                                            <label className="col-sm-3 control-label">{translator.convert('Video series')}</label>
                                            <div className="col-sm-5">
                                                {options.workoutpackagescategories && <Select
                                                    value={formData.video_categories_id}
                                                    isMulti={false}
                                                    name="video_categories_id"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'video_categories_id')}
                                                    options={options.workoutpackagescategories.map(item => {
                                                        return { label: item.title, value: item.instance_id };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('Video Label')}</label>
                                            <div className="col-sm-5">
                                                {/* <select name="muscles" onChange={this.handleFormChange.bind(this)} value={formData.muscles} className="form-control" required>
                                                    <option value="">You belongs to</option>
                                                    {options.muscles && options.muscles.map(item => <option key={Math.random()} value={item.instance_id}>{item.title}</option>)}
                                                </select> */}
                                                {options.video_label && <Select
                                                    value={formData.video_label}
                                                    isMulti={false}
                                                    name="video_label"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'video_label')}
                                                    options={options.video_label.map(item => {
                                                        return { label: item.name, value: item.id };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group required">
                                            <label className="col-sm-3 control-label">{translator.convert('Intensity')}</label>
                                            <div className="col-sm-5">
                                                {/* <select name="muscles" onChange={this.handleFormChange.bind(this)} value={formData.muscles} className="form-control" required>
                                                    <option value="">You belongs to</option>
                                                    {options.muscles && options.muscles.map(item => <option key={Math.random()} value={item.instance_id}>{item.title}</option>)}
                                                </select> */}
                                                {INTENSITY && <Select
                                                    value={formData.intensity}
                                                    isMulti={false}
                                                    name="intensity"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'intensity')}
                                                    options={INTENSITY.map(item => {
                                                        return { label: item.label, value: item.value };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('Cast')}</label>
                                            <div className="col-sm-8">
                                                {formData.cast && formData.cast.length == 0 ? <div className="row margin-t-10"><div className="col-sm-8"><i className="fa fa-plus"></i>
                                                </div></div> : formData.cast.map((item, i) => (<div className="row margin-t-10" key={`steps_${i}`}><div className="col-sm-8"><input className="form-control" name="cast" placeholder={`Cast ${i + 1}`} value={item} onInput={(e) => this.handleRangeChange(e, i)}></input></div><div className="col-sm-4">{i === 0 && <span className="input-group-addon" style={{ width: 50 }} onClick={this.addRange.bind(this)}><i className="fa fa-plus"></i></span>}
                                                    {i !== 0 && <span style={{ width: 50 }} className="input-group-addon" onClick={() => this.removeRange(i)}><i className="fa fa-trash"></i></span>}</div>
                                                </div>))}
                                            </div>
                                        </div>

                                        <div className="form-group required">
                                            <label className="col-sm-3 control-label">{translator.convert('Workout video image')}</label>
                                            <div className="col-sm-9 recipe-image-container">
                                                <p className="text-aqua text-sm">Image size should be greater than <b>800 X 1200 (width X height)</b></p>
                                                <FileBase64
                                                    multiple={false}
                                                    max={1}
                                                    maxSize={10}
                                                    onDone={this.getFiles}
                                                />
                                                {imageFile == '' ? formData.image && formData.image != '' ? <div>
                                                    <PreLoader src={`${S3_MEDIA_URL}/${MEDIA_DIR.WORKOUT}/${formData.image}`} height="200px" width="200px" style={{ marginTop: 10 }} />
                                                </div> : '' : <div>
                                                        <img src={imageFile.base64} height="200px" width="200px" style={{ marginTop: 10 }} alt="noimage" />
                                                    </div>}
                                            </div>
                                        </div>

                                        {/* <div className="form-group required">
                                            <label className="col-sm-3 control-label">{translator.convert('add_to_package')}</label>
                                            <div className="col-sm-5">
                                                <select name="workoutpackage_id" onChange={this.handleFormChange.bind(this)} value={formData.workoutpackage_id} className="form-control" required>
                                                    <option value="">Choose package</option>
                                                    {options.workoutpackages && options.workoutpackages.map(item => <option key={Math.random()} value={item.instance_id}>{item.title}</option>)}
                                                </select>
                                            </div>
                                        </div>

                                         <div className="form-group required">
                                            <label className="col-sm-3 control-label">{translator.convert('order')}</label>
                                            <div className="col-sm-5">
                                                <input type="text" name="instance_order" onChange={this.handleFormChange.bind(this)} value={formData.instance_order} className="form-control" required />
                                            </div>
                                        </div> */}
                                        <div className="form-group required">
                                            <label className="col-sm-3 control-label">{translator.convert('vimeo_code')}</label>
                                            <div className="col-sm-5">
                                                <input type="text" name="video_vimeo" onChange={(e) => {
                                                    this.handleFormChange(e);
                                                    this.onVarifyVimeoCode(e)
                                                }} value={formData.video_vimeo} className="form-control" required />

                                            </div>
                                        </div>
                                        {formData.video_vimeo && <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('preview_image')}</label>
                                            <div className="col-sm-3">
                                                {!smallLoader && vimeoInfo && vimeoInfo.thumbnail_url && <PreLoader src={vimeoInfo.thumbnail_url} alt="loading..." />}
                                                {smallLoader && <img className="img-responsive" src={images.default.loader} alt="loading..." />}
                                                {!smallLoader && vimeoInfo && vimeoInfo.thumbnail_url && <p className="text-aqua text-sm">Title: {vimeoInfo.title}, Uploaded on: {vimeoInfo.upload_date}, By {vimeoInfo.author_name}</p>}
                                                {!smallLoader && !vimeoInfo && <p className="text-danger text-sm">No video available on vimeo</p>}
                                            </div>
                                        </div>}
                                        <div className="form-group required">
                                            <label className="col-sm-3 control-label">{translator.convert('title')}</label>
                                            <div className="col-sm-5">
                                                <input type="text" name="title" onChange={this.handleFormChange.bind(this)} value={formData.title} className="form-control" required />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('tag')}</label>
                                            <div className="col-sm-6">
                                                <CKEditor
                                                    editor={ClassicEditor}
                                                    config={this.editorConfig}
                                                    // data="<p>Hello from CKEditor 5!</p>"
                                                    data={this.tagData}
                                                    onInit={editor => {
                                                        // You can store the "editor" and use when it's needed.
                                                        console.log('Editor is ready to use!', editor);
                                                    }}
                                                    onChange={(event, editor) => {
                                                        const data = editor.getData();
                                                        console.log({ event, editor, data });
                                                        this.tagData = data;
                                                    }}
                                                />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('description')}</label>
                                            <div className="col-sm-6">
                                                <CKEditor
                                                    editor={ClassicEditor}
                                                    config={this.editorConfig}
                                                    data={this.contentData}
                                                    onInit={editor => {
                                                        // You can store the "editor" and use when it's needed.
                                                        console.log('Editor is ready to use!', editor);
                                                    }}
                                                    onChange={(event, editor) => {
                                                        const data = editor.getData();
                                                        console.log({ event, editor, data });
                                                        this.contentData = data;
                                                    }}
                                                />
                                            </div>
                                        </div>

                                        <div className="form-group required ">
                                            <label className="col-sm-3 control-label">{translator.convert('link_exercises_to_this_video')}</label>
                                            <div className="col-sm-5">
                                                {/* <select name="exercises" value={formData.exercises} onChange={this.handleFormChange.bind(this)} className="form-control">
                                                    <option>Look for exercises</option>
                                                    {options.exercises && options.exercises.map(item => <option key={Math.random()} value={item.instance_id}>{item.title}</option>)}
                                                </select> */}
                                                {options.exercises && <Select
                                                    value={formData.exercises}
                                                    isMulti={true}
                                                    name="exercises"
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={(val) => this.handleMultiSelectChange(val, 'exercises')}
                                                    options={options.exercises.map(item => {
                                                        return {
                                                            label: item.title,
                                                            value: item.instance_id,
                                                        };
                                                    })}
                                                />}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('active')}</label>
                                            <div className="col-sm-5">
                                                <div className="icheck-container">
                                                    <RadioGroup name="active" value={'' + formData.active}>
                                                        <Radio
                                                            value="1"
                                                            radioClass="iradio_square-blue local_iradio_square"
                                                            increaseArea="20%"
                                                            label=" <span class='label1'>Active</span>"
                                                            onClick={(e) => this.handleFormChange(e)}
                                                        />
                                                        <Radio
                                                            value="0"
                                                            radioClass="iradio_square-blue local_iradio_square"
                                                            increaseArea="20%"
                                                            label=" <span class='label1'>Deactive</span>"
                                                            onClick={(e) => this.handleFormChange(e)}
                                                        />
                                                    </RadioGroup>
                                                </div>
                                            </div>
                                        </div>

                                        {params.instance_id && displayApproved(videoInfo ? videoInfo : {}, auth) && <div className="form-group">
                                            <label className="col-sm-3 control-label">{translator.convert('approved_by_admin')}</label>
                                            <div className="col-sm-6">
                                                <RadioGroup name="approved" value={'' + formData.approved}>
                                                    <Radio
                                                        value="1"
                                                        name="approved"
                                                        radioClass="iradio_square-blue"
                                                        increaseArea="20%"
                                                        label=" <span class='label1'> Approved </span>"
                                                        onClick={(e) => {
                                                            this.handleFormChange(e);
                                                        }}
                                                    />
                                                    <Radio
                                                        value="0"
                                                        name="approved"
                                                        radioClass="iradio_square-blue local_iradio_square"
                                                        increaseArea="20%"
                                                        label=" <span class='label1'> Waiting </span>"
                                                        onClick={(e) => this.handleFormChange(e)}
                                                    />
                                                    <Radio
                                                        value="2"
                                                        name="approved"
                                                        radioClass="iradio_square-blue local_iradio_square"
                                                        increaseArea="20%"
                                                        label=" <span class='label1'> Reject </span>"
                                                        onClick={(e) => this.handleFormChange(e)}
                                                    />
                                                </RadioGroup>
                                            </div>
                                        </div>}

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="box-footer">
                            <Link className="btn btn-default" to="/manage/workout/videos">{translator.convert('cancel')}</Link>
                            <button type="submit" className={`btn pull-right ${theme.btnClass}`}>{translator.convert('save')}</button>
                        </div>
                    </form>
                </section>
            </div>
        );
    }
}

/* istanbul ignore next */
function mapStateToProps(state) {
    return {
        user: state.user,
        appConfig: state.common.appConfig,
        videos: state.common.videos,
    };
}

export default connect(mapStateToProps)(CreateVideo);
